package com.asiah.pertamina;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class HomeScreen extends AppCompatActivity {

//    ArrayList<String> header1 = new ArrayList<>();
//    ArrayList<String> header2 = new ArrayList<>();

//    private ListView mDrawerList;
//    private List<DrawerItem> mDrawerItems;

    private SwipeRefreshLayout  mRefresh;
    private String              mLastListType;
    private DrawerLayout        mDrawerLayout;
    private DokumenViewAdapter  mAdapter;

    private UserRole mUserRole;
    private String mUserId;

    private Dialog mLoadingDialog;

    public static final String TAG = MainActivity.TAG;

    private TextView mDebugText;

//    private ActionBarDrawerToggle mDrawerToggle;

//    private CharSequence mTitle, mDrawerTitle;

    @Override
    protected void onResume() {
        Log.i(MainActivity.TAG, "onResume: ");
        checkActiveAccount("-1");
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate: homeskrin");
        super.onCreate(savedInstanceState);

        mLoadingDialog = new Dialog(this, R.style.MyDialog);
        mLoadingDialog.setCancelable(false);

        mLoadingDialog.setContentView(R.layout.waiting_server_response);

        setContentView(R.layout.activity_home_screen);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String myName = pref.getString("name", "John Doe");
        mUserId = pref.getString("id", "");
        int userRoleIndex = pref.getInt("role", -1);

        Log.i(MainActivity.TAG, "cek user "+mUserId);
        if (mUserId.equals("")){
            Logout();
        }

        setTitle(myName);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);
//        mTitle = mDrawerTitle = getTitle();

        mDrawerLayout = findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
            new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem) {
                    // Add code here to update the UI based on the item selected
                    // For example, swap UI fragments here
                    Log.i(MainActivity.TAG, "onNavigationItemSelected: " + menuItem.getTitle());
                    String title = menuItem.getTitle().toString();
                    menuItem.setChecked(true);
                    mDrawerLayout.closeDrawers();
                    if (title.equals("Logout")) {
                        Logout();
                    } else if (title.equals("All")){
                        Log.i(TAG, "all");
                        checkActiveAccount("-1");
                    } else if (title.equals("Unread")){
                        Log.i(TAG, "unread");
                        checkActiveAccount("0");
                    } else if (title.equals("Read")){
                        Log.i(TAG, "read ");
                        checkActiveAccount("1");
                    } else if (title.equals("Rejected")){
                        Log.i(TAG, "rejek");
                        checkActiveAccount("2");
                    } else if (title.equals("Approved")){
                        Log.i(TAG, "approve ");
                        checkActiveAccount("3");
                    } else if (title.equals("Change Password")){
                        Log.i(TAG, "ganti passwot ");
                        Intent newintent = new Intent(getBaseContext(), PasswordChanger.class);
                        startActivity(newintent);
                    } else {
                        return false;
                    }
                    return true;
                }
            });

        final View headerLayout = navigationView.getHeaderView(0);
        TextView txt = headerLayout.findViewById(R.id.name);
        TextView role = headerLayout.findViewById(R.id.title);
        txt.setText(myName);
        if (userRoleIndex >= 0) {
            mUserRole = UserRole.values()[userRoleIndex];
            role.setText(mUserRole.toString());

            actionbar.setSubtitle(mUserRole.toString());
        }

//        header1.add("Refinery Development Masterplan Program Balikpapan");
//        header2.add("3 Januari 2020");
//        header1.add("Refinery Development Masterplan Program Cilacap");
//        header2.add("13 Februari 2024");
//        header1.add("Refinery Development Masterplan Program Balongan");
//        header2.add("4 Maret 2021");
//        header1.add("Refinery Development Masterplan Program Dumai");
//        header2.add("23 Mei 2020");
//        header1.add("Refinery Development Masterplan Program Plaju");
//        header2.add("4 September 2021");
//        header1.add("New Grass Root Refinery Tuban");
//        header2.add("24 Juni 2024");
//        header1.add("New Grass Root Refinery Bontang");
//        header2.add("15 Agustus 2023");
//        header1.add("New Grass Root Refinery Balikpapan");
//        header2.add("3 Desember 2025");

        mAdapter = new DokumenViewAdapter(this);//, header1, header2);
//        updateDataList();
        RecyclerView rview = findViewById(R.id.dokumen_view);
        rview.setLayoutManager(new LinearLayoutManager(this));
        rview.setAdapter(mAdapter);
        rview.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));


        mDebugText = findViewById(R.id.debugList);
        mDebugText.setText(FirebaseInstanceId.getInstance().getToken());

        mRefresh = findViewById(R.id.refreshSwipe);
        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshListContract(mLastListType);
            }
        });
//        imeiMatched();
    }





//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        if (requestCode == PERMISSIONS_REQUEST_READ_PHONE_STATE){
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                registerImei();
//            } else {
//                Logout();
//            }
//        }
//    }



    private void checkActiveAccount(final String code){

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String currentImei = pref.getString("imei_no", "");

        HashMap<String, String> params = new HashMap<>();
        params.put("id_user", mUserId);
        params.put("imei", currentImei);

        mLoadingDialog.show();
        NetworkRequest.getInstance().newRequest("check_imei", params, new NetworkRequest.OnNetworkResponse() {
            @Override
            public void Success(JSONObject response) {
                Log.i(TAG, "abis cek imei : "+response.toString());

                try {
                    int respcode = response.getInt("response");
                    if (respcode == 1){
                        refreshListContract(code);
                    } else {
                        Logout();
                    }
                } catch (JSONException e) {
                    Log.i(TAG, "======================");
                    e.printStackTrace();
                    Log.i(TAG, "======================");
                    Logout();
                }
            }

            @Override
            public void Error(VolleyError error) {
                Logout();
            }
        });
    }

    // -1 all
    // 0 : unread
    // 1 : read
    // 2 : rejected
    // 3 : approved
    private void refreshListContract(String code){
        mLastListType = code;
//        RequestQueue queue = Volley.newRequestQueue(this);
//        String url = MainActivity.URL+"get_contract";

        HashMap<String, String> params = new HashMap<>();
        params.put("id_user", mUserId);
        params.put("status", code);

        NetworkRequest.getInstance().newRequest("get_contract", params, new NetworkRequest.OnNetworkResponse() {
            @Override
            public void Success(JSONObject response) {
                mLoadingDialog.hide();
                mAdapter.clearItem();
                try {

                    Log.i(TAG, "response dokumen  "+response.toString());
                    JSONArray rsp = response.getJSONArray("response");
                    SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    for (int i = 0; i < rsp.length(); i++){
                        JSONObject obj = rsp.getJSONObject(i);
                        Log.i(MainActivity.TAG, "----------- "+obj.toString());
                        String title = obj.getString("CONTRACT_TITLE");
                        String created = obj.getString("CREATED_ON");
                        int myId = obj.getInt("CONTRACT_ID");
                        int myStatus = -1;
                        if (mUserRole == UserRole.Legal) {
                            myStatus = obj.getInt("LEGAL_STATUS");
                        } else if (mUserRole == UserRole.Finance){
                            myStatus = obj.getInt("FINANCE_STATUS");
                        } else if (mUserRole == UserRole.Officer){
                            String statusOfficer = obj.getString("OFFICER_CERTIFICATE");
                            if (statusOfficer.equals("")){
                                myStatus = 0;
                            } else {
                                myStatus = Integer.parseInt(statusOfficer);
                            }
                        } else if (mUserRole == UserRole.Vendor){
                            String statusOfficer = obj.getString("VENDOR_CERTIFICATE");
                            if (statusOfficer.equals("")){
                                myStatus = 0;
                            } else {
                                myStatus = Integer.parseInt(statusOfficer);
                            }
                        }
//                                Log.i(MainActivity.TAG, "statusku : "+myStatus);
                        try {
                            Date date = dformat.parse(created);
                            mAdapter.addItem(myId, title, date, myStatus);
                        } catch (ParseException e) {
                            Log.i(TAG, "error parsing document "+rsp.toString());
                            e.printStackTrace();
                        }
//                                Log.i(MainActivity.TAG, title);
                    }

                } catch (JSONException e) {
                    Log.i(TAG, "error retrieve document ");
                    e.printStackTrace();
                }

                updateDataList();
            }

            @Override
            public void Error(VolleyError error) {
                mLoadingDialog.hide();
            }
        });

//        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
////                        int resp = 0;
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        });//
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,3,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
////        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000,3,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        queue.add(stringRequest);
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_dokumen_detail, menu);
//        return super.onCreateOptionsMenu(menu);
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.i(TAG, "onOptionsItemSelected: "+item.getTitle());
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //    private void prepareNavigationDrawerItems() {
//        mDrawerItems = new ArrayList<>();
//        mDrawerItems.add(new DrawerItem(R.string.drawer_icon_linked_in,
//                R.string.drawer_title_linked_in,
//                DrawerItem.DRAWER_ITEM_TAG_LINKED_IN));
//        mDrawerItems.add(new DrawerItem(R.string.drawer_icon_blog,
//                R.string.drawer_title_blog, DrawerItem.DRAWER_ITEM_TAG_BLOG));
//        mDrawerItems.add(new DrawerItem(R.string.drawer_icon_git_hub,
//                R.string.drawer_title_git_hub,
//                DrawerItem.DRAWER_ITEM_TAG_GIT_HUB));
//        mDrawerItems.add(new DrawerItem(R.string.drawer_icon_instagram,
//                R.string.drawer_title_instagram,
//                DrawerItem.DRAWER_ITEM_TAG_INSTAGRAM));
//    }

    private void Logout(){
        SharedPreferences.Editor pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
        pref.putString("id", "");
        pref.commit();

        mLoadingDialog.dismiss();

        Intent newIntent = new Intent(getBaseContext(), MainActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(newIntent);
    }

    private void updateDataList(){
        mRefresh.setRefreshing(false);
        mAdapter.updateView();
        TextView txt = findViewById(R.id.emptyView);
        if (mAdapter.isEmpty()){
            txt.setText("No data available");
            txt.setVisibility(View.VISIBLE);
        } else {
            txt.setVisibility(View.GONE);
        }
    }
}
