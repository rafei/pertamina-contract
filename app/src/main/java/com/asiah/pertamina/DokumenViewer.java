package com.asiah.pertamina;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.gcacace.signaturepad.views.SignaturePad;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;


public class DokumenViewer extends AppCompatActivity implements View.OnTouchListener {

    public static final String TAG = MainActivity.TAG;

    private Dialog          mDialogL, mDialogR;

    private TextView        mRejectionNote, mFingerprintWarning;
    private boolean         mApprover = false;
    private SignaturePad    mSignPad;
    private boolean         mPadSigned = false;

    private String mDocTitle = "";//, mRole = "";
    private int mUserRole, mContractId, mContractStatus;
    private String mUserId;//, mWebViewContent;

    private String[] mExtraLink, mExtraTitles;// = new ArrayList<>();

    FingerprintHandler mFingerHandler;

    private Dialog mLoadingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dokumen_viewer);

        mLoadingDialog = new Dialog(this, R.style.MyDialog);
        mLoadingDialog.setCancelable(false);

        mLoadingDialog.setContentView(R.layout.waiting_server_response);

        mDocTitle = getIntent().getStringExtra("DOC_TITLE");
        mContractId = getIntent().getIntExtra("DOC_ID", -1);
        mContractStatus = getIntent().getIntExtra("DOC_STATUS", -1);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        mUserRole = pref.getInt("role", -1);
        mUserId = pref.getString("id", "");

        //ganti status ke read, klo masih unread

        Log.i(TAG, "statusku sekarang "+mContractId+"  =  "+mContractStatus);
        if (mContractId != -1 && mContractStatus == 0){
            changeStatusContract(1, "", false);
        }
        readDocumentDetail(mContractId);


        mApprover = mUserRole < 5; //3 - finance 4 - legal 5 - officer 6 - vendor
        setTitle(mDocTitle);

//        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        mDialogL = new Dialog(this, R.style.CustomDialogTheme);
        mDialogR = new Dialog(this, R.style.CustomDialogTheme);

        if (mApprover){
            InitApprover();
        } else {
            InitSigner();
        }


        mLoadingDialog.show();
//        changeWebView("<h3>Retrieving data..</h3>");

        mDialogL.setCancelable(true);
        mDialogR.setCancelable(true);

        Button approveBtn = findViewById(R.id.leftBtn);
        Button rejectBtn = findViewById(R.id.rightBtn);
        Button extraBtn = findViewById(R.id.extraBtn);
        extraBtn.setOnTouchListener(this);
        if (mContractStatus <= 1) {
            approveBtn.setOnTouchListener(this);
            rejectBtn.setOnTouchListener(this);


        } else {
            if (mApprover) {
                boolean rejected = mContractStatus == 2;
                approveBtn.setText(rejected ? "REJECTED" : "APPROVED");
                if (rejected){
                    approveBtn.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_rejected), null,null);
                    approveBtn.setTextColor(getResources().getColor(R.color.colorAccent));
                } else {
                    approveBtn.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_check_green), null,null);
                    approveBtn.setTextColor(getResources().getColor(R.color.cpb_green_dark));

                }
            } else {
                approveBtn.setText("SIGNED");
                approveBtn.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_signed), null,null);
                approveBtn.setTextColor(getResources().getColor(R.color.cpb_green_dark));

            }
            approveBtn.setEnabled(false);
            rejectBtn.setVisibility(View.GONE);
        }


        //init web viewer
        WebView webv = findViewById(R.id.dokumenViewer);
        webv.getSettings().setJavaScriptEnabled(true);
        webv.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                mLoadingDialog.show();
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                mLoadingDialog.hide();
                super.onPageFinished(view, url);
            }


        });

        checkActiveAccount();
    }

    private void checkActiveAccount(){

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String currentImei = pref.getString("imei_no", "");

//        RequestQueue queue = Volley.newRequestQueue(this);
//        String url = MainActivity.URL+"check_imei";

        HashMap<String, String> params = new HashMap<>();
        params.put("id_user", mUserId);
        params.put("imei", currentImei);

        NetworkRequest.getInstance().newRequest("check_imei", params, new NetworkRequest.OnNetworkResponse() {
            @Override
            public void Success(JSONObject response) {
                Log.i(TAG, "abis cek imei : "+response.toString());
                try {
                    int respcode = response.getInt("response");
                    if (respcode == 1){

                    } else {
                        //logout
                        Logout();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void Error(VolleyError error) {
                Logout();
            }
        });

//        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.i(TAG, "abis cek imei : "+response.toString());
//
//                        try {
//                            int respcode = response.getInt("response");
//                            if (respcode == 1){
//
//                            } else {
//                                //logout
//                                Logout();
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Logout();
//            }
//        }
//        );
//        queue.add(stringRequest);
    }

    private void Logout(){
        SharedPreferences.Editor pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
        pref.putString("id", "");
        pref.commit();
        Intent newIntent = new Intent(getBaseContext(), MainActivity.class);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(newIntent);
    }

    private void readDocumentDetail(final int doc_id){
//        RequestQueue queue = Volley.newRequestQueue(this);
//        String url = MainActivity.URL+"get_document";

        Log.i(TAG, "baca dokument  "+doc_id);
        HashMap<String, String> params = new HashMap<>();
        params.put("id_contract", String.valueOf(doc_id));
        params.put("id_user", mUserId);

        NetworkRequest.getInstance().newRequest("get_document", params, new NetworkRequest.OnNetworkResponse() {
            @Override
            public void Success(JSONObject response) {
                Log.i(TAG, "baca detail "+response.toString());
                try {
                    JSONArray objArray = response.getJSONArray("response");
                    JSONObject obj = objArray.getJSONObject(0);
                    Log.i(TAG, "tambahan "+objArray.length());

                    JSONArray objAdditional = objArray.getJSONArray(1);
                    Log.i(TAG, "tambahan2 "+objAdditional.toString());

                    int jumlahExtra = objAdditional.length();
                    if (jumlahExtra > 0){
                        mExtraLink = new String[jumlahExtra];
                        mExtraTitles = new String[jumlahExtra];
                        for (int i = 0; i < objAdditional.length(); i++) {
                            JSONObject extraObj = objAdditional.getJSONObject(i);
                            String name = extraObj.getString("NAME");
                            String newUrl = MainActivity.MAIN_URL+"upload/"+name;
                            Log.i(TAG, i+"tambah dok "+newUrl);
                            mExtraLink[i] = newUrl;
                            mExtraTitles[i] = name;
                        }

                    } else {
                        Button extraBtn = findViewById(R.id.extraBtn);
                        extraBtn.setVisibility(View.GONE);
                    }

//                            changeWebView(obj.getString("COMPILED"));
                    updateDocumentView();

                    if (!mApprover){
                        UserRole ur = UserRole.values()[mUserRole];

                        if (ur == UserRole.Officer || ur == UserRole.Vendor){
                            String signature = obj.getString(ur == UserRole.Officer ? "OFFICER_SIGNATURE" : "VENDOR_SIGNATURE");
                            if (!signature.equals("")){
                                mSignPad.setSignatureBitmap(base64ToBitmap(signature));
                            } else {
                                mSignPad.clear();
                            }
                            mSignPad.clear();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i(TAG, "hideme");
                mLoadingDialog.hide();
            }

            @Override
            public void Error(VolleyError error) {
                mLoadingDialog.hide();
            }
        });

//        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
////                        int resp = 0;
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.i(TAG, "get document error  "+error.getMessage());
//                error.printStackTrace();
//            }
//        });//
//        queue.add(stringRequest);
    }

    // TODO: 04-Jun-18 dibuat status enum?
    // 0 : unread, 1 : read, 2 : rejected, 3 : approved
    private void changeStatusContract(final int currentStatus, String note, final boolean refreshAfterFinish){
        Log.i(TAG, "ganti status "+mContractId+"  "+mUserId+"  "+currentStatus);
//        RequestQueue queue = Volley.newRequestQueue(this);
//        String url = MainActivity.URL+"set_status";

        HashMap<String, String> params = new HashMap<>();
        params.put("id_user", String.valueOf(mUserId));
        params.put("id_contract", String.valueOf(mContractId));
        params.put("user_status", String.valueOf(currentStatus));
        params.put("note", note);

        mLoadingDialog.show();

        NetworkRequest.getInstance().newRequest("set_status", params, new NetworkRequest.OnNetworkResponse() {
            @Override
            public void Success(JSONObject response) {
                mLoadingDialog.hide();
                try {
                    Log.i(TAG, "statusku  "+response.toString());
                    int rsp = response.getInt("response");
                    Log.i(TAG, "ini status  "+rsp);

                    if (refreshAfterFinish) {
                        Intent intent = getIntent();
                        finish();
                        if (rsp == 1) {
                            intent.putExtra("DOC_STATUS", currentStatus);
                        }
                        startActivity(intent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void Error(VolleyError error) {

            }
        });

//        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
////                        int resp = 0;
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.i(TAG, "change status error  "+error.getMessage());
//                error.printStackTrace();
//            }
//        });//
//        queue.add(stringRequest);
    }
    // TODO: 04-Jun-18 dibuat status enum?
    // 0 : unread, 1 : read, 2 : rejected, 3 : approved
    private void setCompiled(String signature){

        RequestQueue queue = Volley.newRequestQueue(this);

        String url = MainActivity.URL+"set_compile";

        HashMap<String, String> params = new HashMap<>();
        params.put("id_user", String.valueOf(mUserId));
        params.put("id_contract", String.valueOf(mContractId));
        params.put("signature", signature);

        Log.i(TAG, "set compiled status "+mContractId+"  "+mUserId+" -> ");

        mLoadingDialog.show();

        NetworkRequest.getInstance().newRequest("set_compile", params, new NetworkRequest.OnNetworkResponse() {
            @Override
            public void Success(JSONObject response) {
                mLoadingDialog.hide();
                Log.i(TAG, "ganti signed  "+response.toString());

                Intent intent = getIntent();
                intent.putExtra("DOC_STATUS", 3); //signed
                finish();
                startActivity(intent);
            }

            @Override
            public void Error(VolleyError error) {

            }
        });

//        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
////                        int resp = 0;
////                        try {
//                        mLoadingDialog.hide();
//                            Log.i(TAG, "ganti signed  "+response.toString());
//
//                            Intent intent = getIntent();
//                            intent.putExtra("DOC_STATUS", 3); //signed
//                            finish();
//                            startActivity(intent);
//
////                        } catch (JSONException e) {
////                            e.printStackTrace();
////                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.i(TAG, "compiled yg error  "+error.getMessage());
//                error.printStackTrace();
//            }
//        });//

//        stringRequest.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 10000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return DefaultRetryPolicy.DEFAULT_MAX_RETRIES;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//                Log.e(TAG, "retry "+error.getMessage());
//                error.printStackTrace();
//            }
//        });
//        queue.add(stringRequest);
    }

    public void ClickExtra(View v){
        Intent newIntent = new Intent(this, ExtraViewer.class);
        newIntent.putExtra("LINKS", mExtraLink);
        newIntent.putExtra("ITEMS", mExtraTitles);
        newIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(newIntent);
    }

    public void ClickLeftButton(View view){

//        if (!mApprover) {
//            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
//            String imgString = pref.getString("signature", "");
////            String str = pref.getString("satir", "eot");
////            Log.i(TAG, "ClickLeftButton: "+imgString+" -- "+str);
//
//            if (imgString != "") {
//                Bitmap bmp = base64ToBitmap(imgString);
//                mSignPad.setSignatureBitmap(bmp);
//            }
//        }
        mDialogL.show();
    }

    public void ClickRightButton(View view) {
//        if (mPadSigned) {
        if (mApprover){
            mDialogR.show();
        } else {
            if (mFingerHandler != null){
                mFingerHandler.showFingerprintDialog();
                mDialogR.show();
            } else {
                finishFingerprint();
            }
        }
//        } else {
//            Log.i(TAG, "belum signed");
//        }
    }

    public void ResetDocument(View v){
//        String url = MainActivity.URL+"reset_contract";

        HashMap<String, String> params = new HashMap<>();
        params.put("id_contract", String.valueOf(mContractId));
        params.put("id_user", mUserId);
        params.put("reset_level", mApprover ? "0" : "1");

        mLoadingDialog.show();

        NetworkRequest.getInstance().newRequest("reset_contract", params, new NetworkRequest.OnNetworkResponse() {
            @Override
            public void Success(JSONObject response) {
                mLoadingDialog.dismiss();
                Log.i(TAG, "response reset  "+response.toString());
                finish();
            }

            @Override
            public void Error(VolleyError error) {
                mLoadingDialog.hide();
            }
        });
//        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        mLoadingDialog.dismiss();
//                        Log.i(TAG, "response reset  "+response.toString());
//                        finish();
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.i(TAG, "apanya yg error  "+error.getMessage());
//                error.printStackTrace();
//            }
//        });//
//        Volley.newRequestQueue(this).add(stringRequest);
    }

    private void finishFingerprint(){
        Bitmap bmp = mSignPad.getSignatureBitmap();
        String bes = bitmapToBase64(bmp);

        Log.i(TAG, "bitmapku  "+bes);
        mSignPad.clear();

        setCompiled(bes);

//        finish(); finishnya ada di setcompiled
    }

//    @Override
//    public void onClick(DialogInterface dialog, int which) {
//        Log.i(TAG, "onClick confirm? "+mShowConfirmDialog);
//        NavUtils.navigateUpFromSameTask(this);
//        //do some shit
//    }
//
//
    @Override
    //cuma buat kepentingan ganti warna
    public boolean onTouch(View v, MotionEvent event) {
        Log.i(TAG, "onTouch: "+v.toString());
        switch (event.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                v.animate().alpha(0.3f).setDuration(200).start();
                break;
            case MotionEvent.ACTION_UP:
                v.animate().alpha(1f).setDuration(200).start();
            default :
                v.setAlpha(1f);
        }
        return false;
    }

    private void InitApprover(){
        Button leftButton = findViewById(R.id.leftBtn);
        Button rightButton = findViewById(R.id.rightBtn);

        leftButton.setText("APPROVE");
        rightButton.setText("REJECT");

        mDialogL.setContentView(R.layout.dialog_universal_warning);

        TextView okBtn = mDialogL.findViewById(R.id.dialog_universal_warning_ok);
        TextView cancelBtn = mDialogL.findViewById(R.id.dialog_universal_warning_cancel);


        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeStatusContract(3,"", true);
                mDialogL.dismiss();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogL.dismiss();
            }
        });


        mDialogR.setContentView(R.layout.dialog_reject);
        final TextView okBtnR = mDialogR.findViewById(R.id.dialog_universal_warning_ok);
        mRejectionNote = mDialogR.findViewById(R.id.rejection);
        mRejectionNote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                okBtnR.setEnabled(mRejectionNote.getText().length() > 0);
            }
        });

        okBtnR.setEnabled(mRejectionNote.getText().length() > 0);
        okBtnR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.i(TAG, "onClick: rejek  "+mRejectionNote.getText().toString());
                changeStatusContract(2,mRejectionNote.getText().toString(), true);
                mDialogR.dismiss();
//                goBackHome();
            }
        });


        TextView cancelBtnR = mDialogR.findViewById(R.id.dialog_universal_warning_cancel);
        cancelBtnR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogR.dismiss();
            }
        });
    }

    private void InitSigner(){
        Button leftButton = findViewById(R.id.leftBtn);
        final Button rightButton = findViewById(R.id.rightBtn);

        leftButton.setText("SIGN");
        leftButton.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_sign), null,null);
        leftButton.setTextColor(getResources().getColor(R.color.main_color_grey_700));

        rightButton.setText("VERIFY");
        rightButton.setTextColor(getResources().getColor(R.color.main_color_grey_700));
        rightButton.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_fingerprint), null,null);
        rightButton.setAlpha(0.6f);

        //skip
        rightButton.setVisibility(View.GONE);

        mPadSigned = false;
        rightButton.setEnabled(false);

        mDialogL.setContentView(R.layout.dialog_signature);

        mSignPad = mDialogL.findViewById(R.id.signArea);
        TextView rstBtn = mDialogL.findViewById(R.id.dialog_universal_warning_reset);
        rstBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSignPad.clear();
            }
        });

        mSignPad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                mPadSigned = true;
                rightButton.setEnabled(true);
                rightButton.setAlpha(1);
            }

            @Override
            public void onClear() {
                mPadSigned  = false;
                rightButton.setEnabled(false);
                rightButton.setAlpha(0.6f);
            }
        });


        TextView okBtn = mDialogL.findViewById(R.id.dialog_universal_warning_ok);
        TextView cancelBtn = mDialogL.findViewById(R.id.dialog_universal_warning_cancel);

        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialogL.dismiss();
                //skip
                ClickRightButton(null);
//                finishFingerprint();
//                showFingerprintDialog();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSignPad.clear();
                mDialogL.dismiss();
            }
        });


        Log.i(TAG, "versi  "+Build.VERSION.SDK_INT);

        boolean supportFingerprint = MainActivity.SUPPORT_FINGERPRINT && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
        if (supportFingerprint) {
            mFingerHandler = new FingerprintHandler(this, new FingerprintEvent() {
                @Override
                public void finishFingerprint() {
                    mFingerprintWarning.setText(R.string.fingerprint_success);
                    mFingerprintWarning.setTextColor(getResources().getColor(R.color.cpb_green_dark, null));
                    mFingerprintWarning.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            DokumenViewer.this.finishFingerprint();
                            mDialogR.dismiss();
                        }
                    }, 1000);
                }

                @Override
                public void failedFingerprint(final boolean noSetFingerprint) {
                    mFingerprintWarning.setText(R.string.fingerprint_fail);
                    mFingerprintWarning.setTextColor(getResources().getColor(R.color.colorAccent, null));
                    mFingerprintWarning.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (noSetFingerprint) {
                                mFingerprintWarning.setText(R.string.fingerprint_register);
                                mFingerprintWarning.setTextColor(getResources().getColor(R.color.colorAccent, null));
                            } else {
                                mFingerprintWarning.setText(R.string.fingerprint_sensor);
                                mFingerprintWarning.setTextColor(getResources().getColor(R.color.main_color_grey_600, null));
                            }
                        }
                    }, 3000);
                    Log.i(TAG, "onAuthenticationFailed: ");
                }
            });


            mDialogR.setContentView(R.layout.dialog_fingerprint);
            mFingerprintWarning = mDialogR.findViewById(R.id.warningFingerprint);

            if (!mFingerHandler.isSetFingerprint()) {
                mFingerprintWarning.setText(R.string.fingerprint_register);
                mFingerprintWarning.setTextColor(getResources().getColor(R.color.colorAccent, null));
            }


            TextView cancelBtnR = mDialogR.findViewById(R.id.dialog_universal_warning_cancel);
            cancelBtnR.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mFingerHandler.cancelFingerprint();
                    mDialogR.dismiss();
                }
            });
        }

    }

    private void updateDocumentView(){
        WebView webv = findViewById(R.id.dokumenViewer);

        String content = MainActivity.MAIN_URL+"export/"+mContractId+"_"+mDocTitle+".pdf";
        content = "https://drive.google.com/viewerng/viewer?embedded=true&url=" + content;
        Log.i(TAG, "dokumen url   "+content);
        webv.loadUrl(content);
    }
//    private void changeWebView(String content){
//
//        String [] key = {"[VENDOR_SIGNATURE]","[OFFICER_SIGNATURE]","[COMPANY_NAME_1]", "[COMPANY_NAME_2]" };
//
//        for (int i = 0; i < key.length; i++) {
//            content = content.replace(key[i], "");
//        }
//
//        WebView webv = findViewById(R.id.dokumenViewer);
////        webv.loadUrl("file:///android_asset/fuck.html");
////        webv.loadUrl("https://www.lipsum.com/feed/html");
////        mWebViewContent = content;
////        webv.loadData(content, "text/html; charset=utf-8", "UTF-8");
//        content = "http://floostudio.com/digital_kontrak/app/upload/RKS.pdf";
//        webv.getSettings().setJavaScriptEnabled(true);
//        webv.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + content);
//
//
//    }

//    private void goBackHome(){
//        Intent newIntent = new Intent(getBaseContext(), HomeScreen.class);
//        startActivity(newIntent);
//    }


    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    private Bitmap base64ToBitmap(String b64) {

        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }
}


//interface CallbackStatus {
//    void FinishChange(int status);
//}