package com.asiah.pertamina;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.csform.android.uiapptemplate.font.MaterialDesignIconsTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ExtraViewAdapter extends RecyclerView.Adapter<ExtraViewAdapter.ViewHolder> {

    private static final String TAG = MainActivity.TAG;

    private ArrayList<String> mTitles = new ArrayList<>();
    private ArrayList<String> mLinks = new ArrayList<>();
    private Context mContext;


    public ExtraViewAdapter(Context context){//}, ArrayList<String> mHeader1, ArrayList<String> mHeader2) {
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.extra_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.header1.setText(mTitles.get(position));

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String link = mLinks.get(position);
                String title = mTitles.get(position);

                Intent newIntent = new Intent(mContext, PDFViewer.class);
                newIntent.putExtra("URL", link);
                newIntent.putExtra("TITLE", title);
                mContext.startActivity(newIntent);
            }
        });
    }

    public void addItem(String title, String link){
        mTitles.add(title);
        mLinks.add(link);
    }

    public void updateView(){
        notifyDataSetChanged();
    }

    public void clearItem(){
        mTitles.clear();
    }

    public boolean isEmpty(){
        return mTitles.isEmpty();
    }

    @Override
    public int getItemCount() {
        return mTitles.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView header1;
        ConstraintLayout parentLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.header1 = itemView.findViewById(R.id.headerExtra);
            this.parentLayout = itemView.findViewById(R.id.parent_layout);
        }

    }
}
