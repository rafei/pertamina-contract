package com.asiah.pertamina;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;

public class NetworkRequest {

    public static final String TAG = "NetworkRequest ";
    public final static int MAX_RETRY = 5;

    private static NetworkRequest   instance_;

    private RequestQueue queue_;
    private Context context_;

    private int                     counter_ = 0;

    private NetworkRequest(Context context){
        context_ = context;
        counter_ = 0;
    }

    private NetworkRequest(){}

    public static void init(Context context){
        if (instance_ == null) {
            instance_ = new NetworkRequest(context);
        }
    }

    public static synchronized NetworkRequest getInstance() {
        return instance_;
    }

    public RequestQueue getRequestQueue() {
        if (queue_ == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            queue_ = Volley.newRequestQueue(context_.getApplicationContext());
        }
        return queue_;
    }


    public void newRequest(final String relativeUrl, HashMap<String,String> param, final OnNetworkResponse callbackResponse){
        newRequest(relativeUrl,param == null ? null : new JSONObject(param),callbackResponse);
    }

    public void newRequest(final String relativeUrl, final OnNetworkResponse callbackResponse){
        newRequest(relativeUrl, (JSONObject) null,callbackResponse);
    }


    public void newRequest(final String relativeUrl, final JSONObject param, final OnNetworkResponse callbackResponse){
        RequestQueue queue = getRequestQueue();
        String url = MainActivity.URL+relativeUrl;

        Log.i(TAG, "request  "+url);
        if (param != null){
            Log.i(TAG, "param : "+param.toString());
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, param, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                counter_ = 0;
                Log.i(TAG, "onResponse: "+response.toString());
                callbackResponse.Success(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: 27-Sep-18 handle timeout dan type error lainnya
                Log.i(TAG, "================ onErrorResponse ======================");
                error.printStackTrace();
                Log.i(TAG, "================ onErrorResponse ======================");
                if (error instanceof NoConnectionError){
                    counter_ ++;
                    if (counter_ < MAX_RETRY){ //qqqq hardcode sementara
                        Log.i(TAG, "retry!!  "+counter_);
                        newRequest(relativeUrl, param, callbackResponse);
                        return;
                    }
                }
                callbackResponse.Error(error);
            }

        });
        request.setRetryPolicy(new DefaultRetryPolicy(10000,3,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(request);
    }

    public interface OnNetworkResponse {
        void Success(JSONObject response);
        void Error(VolleyError error);
    }

}
