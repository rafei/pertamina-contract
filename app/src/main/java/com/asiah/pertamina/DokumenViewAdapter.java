package com.asiah.pertamina;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.StringRequest;
import com.csform.android.uiapptemplate.font.MaterialDesignIconsTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DokumenViewAdapter extends RecyclerView.Adapter<DokumenViewAdapter.ViewHolder> {

    private static final String TAG = MainActivity.TAG;

    private ArrayList<Integer> mIds = new ArrayList<>();
    private ArrayList<String> mHeader1 = new ArrayList<>();
    private ArrayList<String> mHeader2 = new ArrayList<>();
    private ArrayList<Integer> mStatus = new ArrayList<>();
    private Context mContext;

    SimpleDateFormat dateFormatList = new SimpleDateFormat("dd MMMM yyyy");


    public DokumenViewAdapter(Context context){//}, ArrayList<String> mHeader1, ArrayList<String> mHeader2) {
//        this.mHeader1 = mHeader1;
//        this.mHeader2 = mHeader2;
        this.mContext = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.dokumen_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.header1.setText(mHeader1.get(position));
        holder.header2.setText(mHeader2.get(position));

        int statusku = mStatus.get(position);
        holder.setStatus(statusku);


        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "onClick: "+position);
                Intent newIntent = new Intent(mContext, DokumenViewer.class);
                newIntent.putExtra("DOC_TITLE", mHeader1.get(position));
                newIntent.putExtra("DOC_ID", mIds.get(position));
                newIntent.putExtra("DOC_STATUS", mStatus.get(position));

//                boolean approver = position % 2 == 0;
//                newIntent.putExtra("ROLE", approver ? "APPROVER" : "SIGNER");
//                newIntent.putExtra("READ", approver);
//                newIntent.putExtra("STATUS", position % 3); //0 standby, 1 approve/sign, 2 reject

                mContext.startActivity(newIntent);
            }
        });
    }

    public void addItem(int id, String title, Date date, int status){
        mHeader1.add(title);
        mHeader2.add(dateFormatList.format(date));
        mStatus.add(status);
        mIds.add(id);

//        Log.i(TAG, "addItem: "+status);
    }

    public void updateView(){
        notifyDataSetChanged();
    }

    public void clearItem(){
        mHeader2.clear();
        mHeader1.clear();
        mStatus.clear();
    }

    public boolean isEmpty(){
        return mHeader2.isEmpty();
    }

    @Override
    public int getItemCount() {
        return mHeader1.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView header1, header2;
        ConstraintLayout parentLayout;
        MaterialDesignIconsTextView iconStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            this.header1 = itemView.findViewById(R.id.header1);
            this.header2 = itemView.findViewById(R.id.date);
            this.parentLayout = itemView.findViewById(R.id.parent_layout);
            this.iconStatus = itemView.findViewById(R.id.statusIcon);
        }

        public void setRead(boolean read){
            header1.setTypeface(read ? Typeface.DEFAULT : Typeface.DEFAULT_BOLD);

            header2.setTypeface(read ? Typeface.DEFAULT : Typeface.DEFAULT_BOLD);
            header2.setTextColor(ResourcesCompat.getColor(itemView.getResources(), read ? R.color.main_color_grey_500 : R.color.colorPrimary, null));
        }

        public void setStatus(int status){
            setRead(status > 0);
            switch (status){
                case 3 : //approved
                    iconStatus.setText(R.string.status_approve);
                    iconStatus.setTextColor(ResourcesCompat.getColor(itemView.getResources(), R.color.cpb_green_dark, null));
                    break;
                case 2: //reject
                    iconStatus.setText(R.string.status_reject);
                    iconStatus.setTextColor(ResourcesCompat.getColor(itemView.getResources(), R.color.colorAccent, null));

                    break;
                default : //pending
                    iconStatus.setText(R.string.status_idle);
                    iconStatus.setTextColor(ResourcesCompat.getColor(itemView.getResources(), R.color.main_color_grey_600, null));
                    break;
            }
        }
    }
}
