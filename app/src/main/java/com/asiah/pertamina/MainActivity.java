package com.asiah.pertamina;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class MainActivity extends AppCompatActivity {

    public static final String TAG = "Pertamina";

    public static final boolean SUPPORT_FINGERPRINT = true;

//    public static String MAIN_URL = "http://www.mor7.com/digital_contract/";

    //    public static final String URL_DEBUG =  "http://floostudio.com/digital_kontrak/app/";
//    public static final String URL_DEPLOY =  "http://www.mor7.com/digital_contract/";
    public static final String URL_DEBUG =  "http://192.168.1.111:8080/digital_contract/";
    public static final String URL_DEPLOY =  "http://apps.floo.id/pertamina/makassar/digitalkontrak/";
    //    public static final String URL_DEPLOY =   "http://floostudio.com/digital_kontrak/app/";
    public static String MAIN_URL = URL_DEPLOY;

//    public static final String API_URL = "api/v2/json_sender/";
    public static final String API_URL = "api/v1/json_sender/";

    public static String URL = MAIN_URL+API_URL;


    private TextView mResponseTxt;
    private TextView mUsername, mPassword;

    private Dialog mLoadingDialog;

    private String mImei = "";

    public static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());


        //qqqqq
//        if (isLoggedIn()){
//            goHome(pref.getString("id", ""), pref.getString("name", "John Doe"), pref.getInt("role", -1), pref.getString("imei_no",""));
//        }

        setContentView(R.layout.login_screen);


        mUsername = findViewById(R.id.field_user);
        mPassword = findViewById(R.id.field_password);


        pref.getString("inputPassword", "");

        mUsername.setText(pref.getString("inputUser", ""));
        mPassword.setText(pref.getString("inputPassword", ""));


        Button rbt = findViewById(R.id.loginBtn);

        rbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PressLogin(v);
            }
        });

        mLoadingDialog = new Dialog(this, R.style.MyDialog);
        mLoadingDialog.setCancelable(false);
        mLoadingDialog.setContentView(R.layout.waiting_server_response);

        mResponseTxt = findViewById(R.id.responseText);
        mResponseTxt.setText("");

//        mDebugText = findViewById(R.id.debugHome);
//        mDebugText.setText(FirebaseInstanceId.getInstance().getToken());

        boolean isChecked = pref.getBoolean("local", false);
        CheckBox cb = findViewById(R.id.serverCheck);
        cb.setChecked(isChecked);

        PressCheckbox(null);
        imeiMatched();

        TextView version = findViewById(R.id.version);
        version.setText(BuildConfig.VERSION_NAME + (SUPPORT_FINGERPRINT ? "-fp":""));

    }


    public void PressLogin(View view){
//        Log.i(TAG, "Press login");
        final String userInput = mUsername.getText().toString();
        final String passwordInput = mPassword.getText().toString();

        if (userInput.isEmpty() || passwordInput.isEmpty()){
            mResponseTxt.setText("Please fill in username and password");
            return;
        }

        showLoading(true);
        mResponseTxt.setText("");

        HashMap<String, String> params = new HashMap<>();
        params.put("username", userInput);
        params.put("password", passwordInput);
        params.put("imei", mImei);

        NetworkRequest.getInstance().newRequest("first_access", params, new NetworkRequest.OnNetworkResponse() {
            @Override
            public void Success(JSONObject response) {
                //                        int resp = 0;
                try {
                    int resp = response.getInt("response");
                    Log.i(TAG, "on login : "+response.toString());

                    if (resp == 1){
                        final int myRole =  response.getInt("role");

                        SharedPreferences.Editor pref = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit();
                        pref.putString("inputUser", userInput);
                        pref.putString("inputPassword", passwordInput);
                        pref.commit();

                        final String respId = response.getString("id");
                        final String respName = response.getString("name");

                        final Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                goHome(respId, respName, myRole, mImei);
                            }
                        }, 1000);

                    } else {
                        showLoading(false);
                        mResponseTxt.setText("Wrong username or password");
                    }
                } catch (JSONException e) {
                    showLoading(false);
                    e.printStackTrace();
                }
            }

            @Override
            public void Error(VolleyError error) {
                Log.i(TAG, "timout??"+error.getMessage());
                showLoading(false);
                mResponseTxt.setText("Request time out. Try again later.");
                error.printStackTrace();
            }
        });
    }

    public void PressCheckbox(View v){
        CheckBox cb = findViewById(R.id.serverCheck);
        boolean checked = cb.isChecked();
        if (cb.getVisibility() == View.GONE){
            checked = false; //qqqqq
        }
        MAIN_URL = checked ? URL_DEBUG : URL_DEPLOY;
        URL = MAIN_URL+API_URL;

        SharedPreferences.Editor pref = PreferenceManager.getDefaultSharedPreferences(this).edit();
        pref.putBoolean("local", cb.isChecked());
        pref.commit();

        Log.i(TAG, "PressCheckbox: "+MAIN_URL);
    }

    private void showLoading(boolean show){
        if (show){
            mLoadingDialog.show();
        } else {
            mLoadingDialog.hide();
        }
//        RobotoTextView loginBtn = findViewById(R.id.login);
//        ProgressBar bar = findViewById(R.id.progressBar);
//        if (show){
//            loginBtn.setVisibility(View.GONE);
//            bar.setVisibility(View.VISIBLE);
//        } else {
//            loginBtn.setVisibility(View.VISIBLE);
//            bar.setVisibility(View.GONE);
//        }
    }

    private boolean isLoggedIn(){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        String myId = pref.getString("id", "");
        Log.i(TAG, "mai id: "+myId);
        if (!myId.equals("")){
            return true;
        }
        return false;
    }

    private void goHome(String id, String name, int role, String imei){

        SharedPreferences.Editor pref = PreferenceManager.getDefaultSharedPreferences(this).edit();
        pref.putString("id", id);
        pref.putString("name", name);
        pref.putString("imei_no", imei);
        pref.putInt("role",role); //dimulai dari 1
        pref.commit();

        String tekken = FirebaseInstanceId.getInstance().getToken();

        Log.i(TAG, "goHome: "+name+"    "+role+"  imei : "+imei+"    tekken "+tekken);

        setToken(this, id, tekken);

        if (mLoadingDialog != null){
            mLoadingDialog.dismiss();
        }

        Intent newIntent = new Intent(this, HomeScreen.class);
        startActivity(newIntent);
    }

    public static void setToken(Context context, String myId, String token){
        if (myId.equals("")){
            Log.i(TAG, "setToken: gagal set token");
            return;
        }
//        RequestQueue queue = Volley.newRequestQueue(context);
//        String url = MainActivity.URL + "set_token";

        HashMap<String, String> params = new HashMap<>();
        params.put("id_user", myId);
        params.put("token", token);

        NetworkRequest.getInstance().newRequest("set_token", params, new NetworkRequest.OnNetworkResponse() {
            @Override
            public void Success(JSONObject response) {
                Log.i(TAG, "response dari token " + response.toString());

            }

            @Override
            public void Error(VolleyError error) {

            }
        });

//        Log.i(TAG, "setToken "+myId+"  ===  "+token);
//        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//
//                        Log.i(TAG, "response dari token " + response.toString());
////                            Log.i(TAG, "simpan imei " + pref.getString("imei",""));
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                error.printStackTrace();
//            }
//        });//
//        queue.add(stringRequest);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_PHONE_STATE){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                TelephonyManager tlpMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                mImei = tlpMgr.getDeviceId();
            } else {
                mImei = "";
            }
        }
    }

    private void imeiMatched(){
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "permintaanku ");
            requestPermissions(new String[]{android.Manifest.permission.READ_PHONE_STATE},
                    PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            TelephonyManager tlpMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            mImei = tlpMgr.getDeviceId();
            Log.i(TAG, "imeiMatched: "+mImei);
//            registerImei();
        }
    }


//    private void registerImei(){
//        TelephonyManager tlpMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//        String imei = tlpMgr.getDeviceId();
//
//
//        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
//        String currentImei = pref.getString("imei_no", "");
//        String username = pref.getString("name", "namaku");
//        Log.i(TAG, username+"  imeiMatched: "+imei +"   --> "+currentImei);
//
//        if (currentImei.equals("") || !currentImei.equals(imei)) {
//            Log.i(TAG, "daftar imei  "+imei);
//            RequestQueue queue = Volley.newRequestQueue(this);
//            String url = MainActivity.URL + "set_imei";
//
//            HashMap<String, String> params = new HashMap<>();
//            params.put("id_user", mUserId);
//            params.put("imei", imei);
//
//            pref.edit().putString("imei_no", imei);
//            pref.edit().commit();
//
////            String isiemi = pref.getString("imei_no","emi");
////            Log.i(TAG, "isiemi "+isiemi);
//
//            JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, url, new JSONObject(params),
//                    new Response.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//
//                            Log.i(TAG, "response dari imei " + response.toString());
////                            Log.i(TAG, "simpan imei " + pref.getString("imei",""));
//                        }
//                    }, new Response.ErrorListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    error.printStackTrace();
//                }
//            });//
//            queue.add(stringRequest);
//        }
//    }
}
