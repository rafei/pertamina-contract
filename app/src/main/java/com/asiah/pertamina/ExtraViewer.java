package com.asiah.pertamina;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;

public class ExtraViewer extends AppCompatActivity {

    ExtraViewAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extra_viewer);
        setTitle("Extra Documents");

        mAdapter = new ExtraViewAdapter(this);

        String[] juduls = getIntent().getStringArrayExtra("ITEMS");
        String[] links = getIntent().getStringArrayExtra("LINKS");

        for (int i = 0; i < juduls.length; i++) {
            mAdapter.addItem(juduls[i], links[i]);
        }

        RecyclerView rview = findViewById(R.id.extra_list);
        rview.setLayoutManager(new LinearLayoutManager(this));
        rview.setAdapter(mAdapter);
        rview.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));


        rview.setAdapter(mAdapter);
    }

}
