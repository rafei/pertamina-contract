package com.asiah.pertamina;

import android.app.Dialog;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class FingerprintHandler {

    public static final String TAG = MainActivity.TAG;

//    private Dialog  mDialogR;


    private Cipher  mDefaultCipher;
    private FingerprintManager mFingerMgr;
    private CancellationSignal mCancellationSignal;

    private KeyStore mKeyStore;
    private KeyGenerator mKeyGenerator;
//    private TextView mFingerprintWarning;


    public boolean isSetFingerprint() {
        return !noSetFingerprint;
    }

    private boolean noSetFingerprint = false;

    static final String DEFAULT_KEY_NAME = "default_key";

    FingerprintEvent mFinishFingerprint;

    private Context mContext;

    public FingerprintHandler(Context context, FingerprintEvent finishFingerprint) {
        mContext = context;
        mFinishFingerprint = finishFingerprint;



        try {
            mKeyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (KeyStoreException e) {
            throw new RuntimeException("Failed to get an instance of KeyStore", e);
        }
        try {
            mKeyGenerator = KeyGenerator
                    .getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get an instance of KeyGenerator", e);
        }

        try {
            mDefaultCipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
                    + KeyProperties.BLOCK_MODE_CBC + "/"
                    + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get an instance of Cipher", e);
        }
//**//


        //* FINGAPRINT *//
        mFingerMgr = mContext.getSystemService(FingerprintManager.class);
        if (!mFingerMgr.hasEnrolledFingerprints()) {

            //bikin dialog suruh catet fingerprint
            noSetFingerprint = true;
            return;
        }
        createKey(DEFAULT_KEY_NAME, true);

        noSetFingerprint = false;
    }

    public void cancelFingerprint(){
        if (mCancellationSignal != null) {
            mCancellationSignal.cancel();
        }
    }

    public void showFingerprintDialog() {
        //* FINGAPRINT *//
        if (initCipher(mDefaultCipher, DEFAULT_KEY_NAME)) {
            FingerprintManager.CryptoObject cobj = new FingerprintManager.CryptoObject(mDefaultCipher);
            mCancellationSignal = new CancellationSignal();
            mFingerMgr.authenticate(cobj, mCancellationSignal, 0, new FingerprintManager.AuthenticationCallback() {
                @Override
                public void onAuthenticationError(int errorCode, CharSequence errString) {
                    Log.i(TAG, "onAuthenticationError: ");

                    super.onAuthenticationError(errorCode, errString);
                }

                @Override
                public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
                    Log.i(TAG, "onAuthenticationHelp: ");
                    super.onAuthenticationHelp(helpCode, helpString);
                }

                @Override
                public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
                    Log.i(TAG, "onAuthenticationSucceeded: ");
                    mFinishFingerprint.finishFingerprint();

                }

                @Override
                public void onAuthenticationFailed() {
                    mFinishFingerprint.failedFingerprint(noSetFingerprint);
                    super.onAuthenticationFailed();
                }
            }, null);

        }

        //**//
    }

    //* FINGAPRINT *//
    public void createKey(String keyName, boolean invalidatedByBiometricEnrollment) {
        // The enrolling flow for fingerprint. This is where you ask the user to set up fingerprint
        // for your flow. Use of keys is necessary if you need to know if the set of
        // enrolled fingerprints has changed.
        try {
            mKeyStore.load(null);
            // Set the alias of the entry in Android KeyStore where the key will appear
            // and the constrains (purposes) in the constructor of the Builder

            KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(keyName,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    // Require the user to authenticate with a fingerprint to authorize every use
                    // of the key
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7);

            // This is a workaround to avoid crashes on devices whose API level is < 24
            // because KeyGenParameterSpec.Builder#setInvalidatedByBiometricEnrollment is only
            // visible on API level +24.
            // Ideally there should be a compat library for KeyGenParameterSpec.Builder but
            // which isn't available yet.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                builder.setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment);
            }
            mKeyGenerator.init(builder.build());
            mKeyGenerator.generateKey();
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private boolean initCipher(Cipher cipher, String keyName) {
        try {
            mKeyStore.load(null);
            SecretKey key = (SecretKey) mKeyStore.getKey(keyName, null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

}
//**//
interface FingerprintEvent {
    void finishFingerprint();
    void failedFingerprint(boolean notsetFingerprint);
}
