package com.asiah.pertamina;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import com.onesignal.OneSignal;

import io.sentry.Sentry;
import io.sentry.android.AndroidSentryClientFactory;

public class PertaminaApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
//        OneSignal.startInit(this)
//                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
//                .unsubscribeWhenNotificationsAreDisabled(true)
//                .init();

        NetworkRequest.init(this);
        initSentry();
    }


    private void initSentry() {
//        String sentryDSN    = "https://42303adcd03048288808005726ecaa2a@sentry.io/1323442";
        String sentryDSN    = "https://7ddf255b0e034079bb62a125eced571c@sentry.io/1369157";
        Sentry.init(sentryDSN, new AndroidSentryClientFactory(this.getApplicationContext()));
    }
}
