package com.asiah.pertamina;

public enum UserRole {
    NoRole, Superadmin, Admin, Finance, Legal, Officer, Vendor
}
