package com.asiah.pertamina;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class PDFViewer extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfviewer);
//        PDFView view = findViewById(R.id.pdfView);
//        view.fromUri(Uri.parse("http://localhost/pdf/rks.pdf")).load();
//        view.fromAsset("pdftest.pdf").load();
        WebView wb = findViewById(R.id.pdfreader);

        wb.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });
        wb.getSettings().setJavaScriptEnabled(true);

        String pdfUrl = getIntent().getStringExtra("URL");
        setTitle(getIntent().getStringExtra("TITLE"));

        Log.i(MainActivity.TAG, "urlku "+pdfUrl);
        wb.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=" + pdfUrl);
    }
}
